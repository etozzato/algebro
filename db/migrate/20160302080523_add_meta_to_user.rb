class AddMetaToUser < ActiveRecord::Migration
  def change
    add_column :users, :equations_count, :integer
    add_column :users, :score, :float, default: 0
    add_column :users, :correct_count, :integer,  default: 0
    add_column :users, :incorrect_count, :integer,  default: 0
    add_column :users, :forfeit_count, :integer,  default: 0
  end
end
