class CreateEquations < ActiveRecord::Migration
  def change
    create_table :equations do |t|
      t.references :user

      t.integer  :a
      t.integer  :b
      t.float    :difficulty

      t.integer  :delta1
      t.integer  :delta2
      t.integer  :result
      t.integer  :response
      t.boolean  :is_correct
      t.float    :solve_time, default: 0

      t.string :op

      t.datetime :start_at, null: true
      t.datetime :finish_at, null: true

      t.timestamps null: false
    end
  end
end
