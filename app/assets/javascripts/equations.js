//  binds off and fade buttons
var bindUIButtons = function(){

  $('.fade-ing').on('click', function(){
    $(this).addClass('faded');
  });

  $('.hide-ing').on('click', function(){
    $(this).addClass('hide');
  });

  $('.reveal-able').on('click', function(){
    $('.reveal').addClass('revealed');
  });

  $('.speak-able').on('click', function(){
    read();
  });

  $('.speak-ing').on('click', function(){
    window.say($(this).data('text'));
  });

};

var bindTimeEquation = function(){

 $('.timer-on').on('click', function(){
   window.ms = setInterval(myCallback, 50);
   function myCallback() {
     var val = 0.05 * ($('#equation_solve_time').get(0).value++);
     $('span.timer').html(val.toFixed(2));
     }
 });

 $('.timer-off').on('click', function(){
   clearInterval(window.ms);
 });

};

var bindCompleteEquation = function(){

  $('.submit-response').on('click', function(){

    $('#equation_response').val($(this).data('value'));
    $('form.edit_equation').submit();

  });

};

// finds any data-text in elements of class readable
// concatenates and sends to speech api
var read = function(){

  var text = ($('.speak').map(function(val,i){
    return $(i).data('text')
  })).toArray();

  if(text.length > 0){
    window.say(text.join(", "));
  }

};

// initializes bindings after page load or turbolinks
var ready = function(){
  bindUIButtons();
  bindTimeEquation();
  bindCompleteEquation();
};

$(document).ready(ready);
$(document).on('page:load', ready);
