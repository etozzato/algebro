class EquationsController < ApplicationController

  # before_action :authenticate_user!

  def new
    flash[:info] = "You're using <em>Algebro</em> as a <strong>guest</strong>: Sign in or Sign up to keep your score! Also, make sure your <b>volume</b> is up!".html_safe unless user_signed_in?
    @equation = Equation.new
    @equation.save
  end

  def update
    @equation = Equation.find(params[:id])
    @equation.update(equation_params.merge({
      user_id: user_signed_in? ? current_user.id : nil,
      finish_at: Time.now,
      solve_time: (equation_params['solve_time'].to_f * 50)
      }))
    redirect_to equation_path(@equation)
  end

  def show
    flash[:info] = "You're using <em>Algebro</em> as a <strong>guest</strong>: Sign in or Sign up to keep your score! Also, make sure your <b>volume</b> is up!".html_safe unless user_signed_in?
    @equation = Equation.find(params[:id])
  end

  private

  def equation_params
    params.require(:equation).permit(:id, :response, :start_at, :solve_time)
  end

end
