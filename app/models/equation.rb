class Equation < ActiveRecord::Base

  belongs_to :user, counter_cache: true

  before_save :evaluate_response, if: :start_at_changed?
  after_update :update_user_score

  SCORES = {
    correct: 2,
    incorrect: -2,
    forfeit: -1
  }

  MAX_INT   = 12
  MIN_INT   = 0
  MAX_DELTA = 5

  Operations = {
    0 => '+',
    1 => '-'
  }

  def initialize
    super
    self.a = rand(MAX_INT)
    self.b = rand(MAX_INT)
    self.op = operation(a,b)

    self.result = eval("#{a}#{op}#{b}")

    self.delta1 = rand(MAX_DELTA + 1)
    self.delta2 = rand(MAX_DELTA - 1)
    while delta1 == delta2
      self.delta2 = rand(MAX_DELTA)
    end
  end

  def operation(a,b)
    if a > b
      Equation::Operations[rand(100) % 2]
    else
      '+'
    end
  end

  def options
    @options ||= [
      result,
      eval("#{result}#{operation(a,b)}#{delta1}"),
      eval("#{result}#{operation(a,b)}#{delta2}")
    ].shuffle
  end

  def response_score
    SCORES[response_grade]
  end

  def op_class
    op == '-' ? :danger : :success
  end

  def op_text
    op == '-' ? :minus : :plus
  end

  def response_time
    (finish_at - start_at).to_f
  end

  private

  def evaluate_response
    update_column(:is_correct, response == result)
  end

  def update_user_score
    user.update_score(response_score, response_grade) unless user.nil?
  end

  def response_grade
    return :forfeit if response.nil?
    is_correct ? :correct : :incorrect
  end
end
