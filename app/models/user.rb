class User < ActiveRecord::Base
  enum role: [:user, :vip, :admin]
  after_initialize :set_default_role, :if => :new_record?

  has_many :equations

  def set_default_role
    self.role ||= :user
  end

  def update_score(equation_score, response_grade)
    puts "update_score(#{equation_score}, #{response_grade})"
    new_score = score + equation_score
    new_score = 0 if new_score < 0
    update_column(:score, new_score)
    increment("#{response_grade}_count".to_sym)
  end

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :invitable, :database_authenticatable, :registerable, :confirmable,
         :recoverable, :rememberable, :trackable, :validatable
end
