# Ability binding

## Class **speak-able**
when clicked, makes **speak** go to synth

## Class **speak**
contains text in the data-text attribute to be collected,
concatenated and sent to the speech synth.

## Class **speak-ing**
when clicked sends the data-text attribute to the speech synth.

## Class **reveal-able**
when clicked, makes **reveal** fadeIn

# Callback binding

## Class **fade-ing**
when clicked, fades to .25%

## Class **hide-ing**
when clicked, disappears

# UI state

## Class reveal
is hidden

## Class faded
opacity: .25
