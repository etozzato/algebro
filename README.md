Algebro
==============

I wanted to help my kids to practice algebra.. So I _gamified_ the process.
**Gamification** is the concept of applying game mechanics and game design techniques to engage and motivate people to achieve their goals.

https://algebro.herokuapp.com/
